from django.core.management.base import BaseCommand
# , CommandError

import argparse
import datetime

from sync import archiver
from ifq import scraper


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        # parser.add_argument('rando', nargs='+', type=int)
        # Named (optional) arguments
        parser.add_argument(
            '--pub_date',
            # action='store_true',
            dest='pub_date',
            default=datetime.date.today(),
            help='Newpaper date - format YYYY-MM-DD',
            required=False,
            type=valid_date)

    def handle(self, *args, **options):

        pub_date = options['pub_date']

        # print('pub_date', pub_date)

        exists = archiver.exists(pub_date)

        # print('exists', exists)

        if exists:
            self.stdout.write('Already saved in the archive')
            return

        try:
            local_filename = scraper.download_pdf(pub_date)

            self.stdout.write('Uploading file ' + local_filename)
            archiver.put_file(pub_date, local_filename)

        except Exception as e:
            import sys, traceback
            self.stderr.write('Something is gone bad')
            traceback.print_exc(file=sys.stderr)
            


def valid_date(s):
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)
