# -*- coding: utf-8 -*-

from django.conf import settings

import dropbox

client = dropbox.Dropbox(settings.DROPBOX_ACCESS_TOKEN)


def exists(pub_date):
    """
    Check if the newspaper is available for the pub date.

        access_token = settings.DROPBOX_ACCESS_TOKEN

        client = dropbox.client.DropboxClient(access_token)
        print('linked account: ', client.account_info())

        folder_metadata = client.metadata('/Il Fatto Quotidiano')
        print "metadata:", folder_metadata

        self.stdout.write('Successfully run an no-op command ')
    """

    filename = pub_date.strftime(settings.SYNC_FILENAME_PATTERN)

    print("Looking for %s in the Dropbox archive .. " % (pub_date))

    return client.files_search(settings.DROPBOX_ROOT_FOLDER, filename).matches


def put_file(pub_date, local_filename):

    filename = pub_date.strftime(settings.SYNC_FILENAME_PATTERN)

    with open(local_filename, "rb") as f:
        client.files_upload(f.read(), '%s/%s' % (settings.DROPBOX_ROOT_FOLDER, filename), mute = True)

