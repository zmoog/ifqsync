# -*- coding: utf-8 -*-

from django.conf import settings

import logging
import os
import requests
from lxml import html

IFQ_LOGIN_URL = 'https://shop.ilfattoquotidiano.it/login/'
IFQ_ARCHIVE_URL = 'https://shop.ilfattoquotidiano.it/archivio-edizioni/'

logging.basicConfig(level=logging.DEBUG)


def download_pdf(pub_date):

    login_payload = dict(
        username=settings.IFQ_USERNAME,
        password=settings.IFQ_PASSWORD,
        _wp_http_referer='/login/',
        redirect='/login/',
        login='Accedi')

    edition_payload = dict(
        # edition_date='30/05/2015',
        edition_date=pub_date.strftime('%d/%m/%Y'),
        # edition_date_nonce=edition_date_nonce,
        _wp_http_referer='/abbonati/')

    # logging.debug('login_payload: {}'.format(login_payload))

    with requests.Session() as session:

        resp = session.get(IFQ_LOGIN_URL)
        # print(resp.status_code) print(resp.cookies)
        tree = html.fromstring(resp.text)
        _wpnonce = tree.xpath('//input[@id="_wpnonce"]')
        # print('_wpnonce', _wpnonce)
        login_payload['_wpnonce'] = _wpnonce[0].value

        # cookies = dict(cookies_are='working')
        resp = session.post(IFQ_LOGIN_URL, data=login_payload)
        # resp = session.post(TEST_URL, data=payload)  # , cookies=cookies)

        # print(resp.status_code)
        # logging.debug(resp.text)

        resp = session.get(IFQ_ARCHIVE_URL)
        # logging.debug(resp.status_code)
        # logging.debug(resp.text)

        tree = html.fromstring(resp.text)
        _wpnonce = tree.xpath('//input[@name="edition_date_nonce"]')
        # print('_wpnonce', _wpnonce)
        edition_date_nonce = _wpnonce[0].value

        edition_payload['edition_date_nonce'] = edition_date_nonce

        logging.debug(edition_payload)

        resp = session.post(IFQ_ARCHIVE_URL,
                            data=edition_payload,
                            stream=True)

        # logging.debug(resp.status_code)

        if resp.status_code != 200:
            # print(resp.text)
            raise Exception()

        local_filename = pub_date.strftime(
            os.path.join(settings.SYNC_TMP_PATH,
                         settings.SYNC_FILENAME_PATTERN))

        with open(local_filename, 'wb') as f:
            for chunk in resp.iter_content(chunk_size=1024):
                # filter out keep-alive new chunks
                if chunk:
                    f.write(chunk)
                    f.flush()

        return local_filename
